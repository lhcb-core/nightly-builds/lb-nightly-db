==============================================
Changes of CouchDB Schema since LbNightlyTools
==============================================

With the migration to the new Nightly Build System, we did some change to the
CouchDB schema.

Databases and Document Names
----------------------------

In `lhcb-core/nightly-builds/lb-nightly-db!2
<https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db/-/merge_requests/2>`_
we switched from one CouchDB database per builds flavour to one database with
one `partition <https://docs.couchdb.org/en/stable/partitioned-dbs/index.html>`_
per flavour.

This required a change of the rule used to construct the document names and to
the database name, from::

    {server_url}/nightly-{flavour}/{slot_name}.{build_id}

to::

    {server_url}/nightly-builds/{flavour}:{slot_name}:{build_id}


Minor fixes
-----------

- `lhcb-core/nightly-builds/lb-nightly-db!6
  <https://gitlab.cern.ch/lhcb-core/nightly-builds/lb-nightly-db/-/merge_requests/6>`_:
  change the checkout report fields for warnings and errors from ``warning`` and
  ``error`` to ``warnings`` and ``errors`` (same as for the corresponding fields
  in builds reports
