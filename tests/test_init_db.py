###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import datetime
from unittest.mock import Mock, call, patch

import pytest
from requests import HTTPError

from lb.nightly.db.utils import init_db

patch_conf = patch(
    "lb.nightly.configuration",
    **{
        "service_config.return_value": {
            "couchdb": {"url": "https://frodo:friend@some-host.example.org/db-nightly"}
        }
    },
)

patch_couchdb = patch("cloudant.client.CouchDB")


@patch_conf
@patch_couchdb
def test_basic(cdb, _conf):
    init_db()
    print(cdb.mock_calls)

    cdb().create_database.assert_any_call("db-nightly", partitioned=True)
    db = cdb().create_database()
    assert any(
        c[0][0]["_id"] == "_design/summaries" for c in db.create_document.call_args_list
    )


@patch_conf
@patch_couchdb
def test_db_exists(cdb, _conf):
    cdb().__contains__.return_value = True

    init_db()
    print(cdb.mock_calls)

    cdb().__getitem__.assert_any_call("db-nightly")

    db = cdb().__getitem__()
    assert any(
        c[0][0]["_id"] == "_design/summaries" for c in db.create_document.call_args_list
    )


@patch_conf
@patch_couchdb
def test_view_exists(cdb, _conf):
    cdb().create_database().__contains__.return_value = True

    init_db()
    print(cdb.mock_calls)

    cdb().create_database.assert_any_call("db-nightly", partitioned=True)
    db = cdb().create_database()
    db.create_document.assert_not_called()


@patch_conf
@patch_couchdb
def test_view_overwrite(cdb, _conf):
    cdb().create_database().__contains__.return_value = True

    init_db(overwrite=True)
    print(cdb.mock_calls)

    cdb().create_database.assert_any_call("db-nightly", partitioned=True)
    db = cdb().create_database()
    db.__getitem__.assert_called()
    db.__getitem__().update.assert_called()
    db.__getitem__().save.assert_called()
