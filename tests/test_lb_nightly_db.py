###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from pathlib import Path

import tomli

from lb.nightly.db import __version__


def test_version():
    root_dir = Path(__file__).parent.parent
    with open(root_dir / "pyproject.toml", "rb") as infile:
        pyproject = tomli.load(infile)
    assert __version__ == pyproject["tool"]["poetry"]["version"]
