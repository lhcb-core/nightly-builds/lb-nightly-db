###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import datetime
from unittest.mock import MagicMock, Mock, call, patch

import pytest
from lb.nightly.configuration import Project, Slot
from requests import HTTPError

from lb.nightly.db.database import Database

from .test_database import MockDBData


def test_lastBuildId():
    def mock_view_result(partition, design_doc, view, **kwargs):
        assert view == "latest_builds"
        assert kwargs.get("group") is True

        return (
            {
                "nightly": {"slot-name": [{"value": [{"max": 10}, None]}]},
                "testing": {"new-slot": [{"value": [{"max": 123}, None]}]},
            }
            .get(partition, {})
            .get(kwargs["key"], [])
        )

    mock_db = MagicMock()
    mock_db.get_partitioned_view_result = mock_view_result

    db = Database(mock_db)

    assert db.lastBuildId("slot-name") == 10
    assert db.lastBuildId("new-slot") == 0

    assert db.lastBuildId("new-slot", flavour="testing") == 123


def test_latestSlotsBuilt():
    def mock_view_result(partition, design_doc, view, **kwargs):
        assert view == "latest_builds"
        assert kwargs.get("group") is True

        return {
            "nightly": [
                {
                    "key": "dev-slot",
                    "value": [{"max": 7}, {"max": 1598918400000}],  # 2020-09-01
                },
                {
                    "key": "fancy-slot",
                    "value": [{"max": 11}, {"max": 1601510400000}],  # 2020-10-01
                },
            ],
            "testing": [
                {"key": "dev-slot", "value": [{"max": 1}, {"max": 1234}]},
                {"key": "test-slot", "value": [{"max": 5}, {"max": 1234}]},
            ],
        }.get(partition, [])

    mock_db = MagicMock()
    mock_db.get_partitioned_view_result = mock_view_result

    db = Database(mock_db)

    assert db.latestSlotsBuilt() == {
        "dev-slot": {"build_id": 7, "id": "nightly:dev-slot:7"},
        "fancy-slot": {"build_id": 11, "id": "nightly:fancy-slot:11"},
    }

    assert db.latestSlotsBuilt(flavour="testing") == {
        "dev-slot": {"build_id": 1, "id": "testing:dev-slot:1"},
        "test-slot": {"build_id": 5, "id": "testing:test-slot:5"},
    }

    assert db.latestSlotsBuilt(since=datetime.date(2010, 9, 1)) == {
        "dev-slot": {"build_id": 7, "id": "nightly:dev-slot:7"},
        "fancy-slot": {"build_id": 11, "id": "nightly:fancy-slot:11"},
    }

    assert db.latestSlotsBuilt(since=datetime.date(2020, 10, 1)) == {
        "fancy-slot": {"build_id": 11, "id": "nightly:fancy-slot:11"},
    }

    assert db.latestSlotsBuilt(since=datetime.date(2030, 10, 1)) == {}


def test_slotsForDay():
    def mock_view_result(partition, design_doc, view, **kwargs):
        assert view == "by_day"
        assert not kwargs.get("group")
        assert kwargs.get("key") == "2020-10-01"

        return {
            "nightly": [
                {
                    "key": "2020-10-01",
                    "id": "nightly:some-slot:9",
                    "value": {
                        "slot": "some-slot",
                        "build_id": 9,
                        "projects": ["a", "b", "c"],
                        "platforms": ["1", "2", "3"],
                    },
                },
            ],
            "testing": [],
        }.get(partition, [])

    mock_db = MagicMock()
    mock_db.get_partitioned_view_result = mock_view_result

    db = Database(mock_db)

    expected = [
        {
            "slot": "some-slot",
            "build_id": 9,
            "projects": ["a", "b", "c"],
            "platforms": ["1", "2", "3"],
        }
    ]
    assert db.slotsForDay(datetime.date(2020, 10, 1)) == expected
    assert db.slotsForDay("2020-10-01") == expected


def test_slotsSinceDay():
    def mock_view_result(partition, design_doc, view, **kwargs):
        assert view == "by_day"
        assert not kwargs.get("group")
        assert "startkey" in kwargs

        return {
            "nightly": [
                data
                for data in [
                    {
                        "key": "2020-10-01",
                        "id": "nightly:some-slot:9",
                        "value": {
                            "slot": "some-slot",
                            "build_id": 9,
                            "projects": ["a", "b", "c"],
                            "platforms": ["1", "2", "3"],
                        },
                    },
                    {
                        "key": "2020-10-02",
                        "id": "nightly:some-slot:10",
                        "value": {
                            "slot": "some-slot",
                            "build_id": 10,
                            "projects": ["a", "b", "c"],
                            "platforms": ["1", "2", "3"],
                        },
                    },
                    {
                        "key": "2020-10-02",
                        "id": "nightly:new-slot:1",
                        "value": {
                            "slot": "new-slot",
                            "build_id": 1,
                            "projects": ["a"],
                            "platforms": ["1"],
                        },
                    },
                ]
                if data["key"] >= kwargs["startkey"]
            ],
            "testing": [],
        }.get(partition, [])

    mock_db = MagicMock()
    mock_db.get_partitioned_view_result = mock_view_result

    db = Database(mock_db)

    expected = [
        (
            "2020-10-01",
            {
                "slot": "some-slot",
                "build_id": 9,
                "projects": ["a", "b", "c"],
                "platforms": ["1", "2", "3"],
            },
        ),
        (
            "2020-10-02",
            {
                "slot": "some-slot",
                "build_id": 10,
                "projects": ["a", "b", "c"],
                "platforms": ["1", "2", "3"],
            },
        ),
        (
            "2020-10-02",
            {
                "slot": "new-slot",
                "build_id": 1,
                "projects": ["a"],
                "platforms": ["1"],
            },
        ),
    ]

    assert db.slotsSinceDay(datetime.date(2020, 10, 1)) == expected
    assert db.slotsSinceDay("2020-10-01") == expected

    assert db.slotsSinceDay("2020-10-02") == expected[1:]

    assert db.slotsSinceDay("2020-10-03") == []


def test_get_artifact_summary():
    mock_db = MagicMock()
    mock_db.get_view_result().all.return_value = [
        {"value": {"artifact": "something.zip"}}
    ]

    db = Database(mock_db)
    assert db.get_artifact_summary("something.zip") == {"artifact": "something.zip"}

    mock_db = MagicMock()
    mock_db.get_view_result().all.return_value = []

    db = Database(mock_db)
    assert db.get_artifact_summary("something.zip") is None


def test_slotBuilds():
    mock_db = MagicMock()
    mock_db.get_partitioned_query_result.return_value = [
        {"build_id": 10, "date": "2020-10-20"},
        {"build_id": 11, "date": "2020-10-21"},
    ]

    db = Database(mock_db)

    assert db.slotBuilds("name", flavour="flavour", min_id=10) == [
        (10, "2020-10-20"),
        (11, "2020-10-21"),
    ]


def test_getAll():
    def f(**kwargs):
        assert kwargs["include_docs"]
        assert "keys" in kwargs
        return {"rows": [{"id": key, "doc": {"_id": key}} for key in kwargs["keys"]]}

    mock_db = MagicMock()
    mock_db.all_docs = f

    db = Database(mock_db)

    assert list(doc["_id"] for doc in db.getAll(["a", "b", "c"])) == ["a", "b", "c"]


def test_slotDocs():
    value = [
        {"slot": "lhcb-release", "build_id": 2},
        {"slot": "lhcb-release", "build_id": 1},
    ]

    mock_db = MagicMock()
    mock_db.get_partitioned_query_result.return_value = value

    db = Database(mock_db)

    assert db.slotDocs("lhcb-release", flavour="release") is value
    mock_db.get_partitioned_query_result.assert_called_once_with(
        "release", selector={"slot": "lhcb-release"}, sort=[{"build_id": "desc"}]
    )
