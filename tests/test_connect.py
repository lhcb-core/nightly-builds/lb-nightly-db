###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from datetime import datetime
from unittest.mock import patch

import pytest

from lb.nightly.db import connect
from lb.nightly.db.database import Database


@patch("cloudant.client.CouchDB")
def test_basic(cdb):
    assert isinstance(
        connect(db_url="http://me:123@ice.cream/strawberry"),
        Database,
    )

    print(cdb.mock_calls)
    cdb.assert_called_with(
        user="me",
        auth_token="123",
        url="http://ice.cream",
        connect=True,
        admin_party=False,
    )
    cdb().__getitem__.assert_called_with("strawberry")


@patch(
    "lb.nightly.configuration",
    **{
        "service_config.return_value": {
            "couchdb": {"url": "https://frodo:friend@some-host.example.org/nightly"}
        }
    },
)
@patch("cloudant.client.CouchDB")
def test_from_config(cdb, _conf):
    assert isinstance(connect(), Database)
    print(cdb.mock_calls)
    cdb.assert_called_with(
        user="frodo",
        auth_token="friend",
        url="https://some-host.example.org",
        connect=True,
        admin_party=False,
    )
    cdb().__getitem__.assert_called_with("nightly")


@patch("cloudant.client.CouchDB")
def test_unauthenticated(cdb):
    assert isinstance(
        connect(db_url="http://ice.cream/strawberry"),
        Database,
    )

    print(cdb.mock_calls)
    cdb.assert_called_with(
        user=None,
        auth_token=None,
        url="http://ice.cream",
        connect=True,
        admin_party=True,
    )
    cdb().__getitem__.assert_called_with("strawberry")


def test_unsupported_url():
    with pytest.raises(ValueError):
        connect(db_url="ssh://user:pass@stuff/dbname")


@patch(
    "lb.nightly.configuration",
    **{"service_config.side_effect": FileNotFoundError()},
)
@patch("cloudant.client.CouchDB")
def test_config_file_missing(cdb, _conf):
    with pytest.raises(KeyError):
        connect()


@patch("cloudant.client.CouchDB")
def test_reconnect(cdb):
    db = connect(db_url="http://me:123@ice.cream/strawberry")
    assert isinstance(db, Database)

    cdb()["strawberry"].client.connect.assert_not_called()

    # force DB to reconnect
    db._db_last_access = datetime.fromtimestamp(0)
    db._db

    cdb()["strawberry"].client.connect.assert_called_once()

    # no reconnection here
    db._db
    cdb()["strawberry"].client.connect.assert_called_once()
