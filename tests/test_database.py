###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import datetime
from unittest.mock import MagicMock, Mock, call, patch

import pytest
from cloudant.error import CloudantDatabaseException
from lb.nightly.configuration import DBASE, Package, Project, Slot
from requests import HTTPError

from lb.nightly.db.database import Database


class MockDBData(Mock):
    """
    Helper class to mock the content of a database.
    """

    def _get_child_mock(self, **kw):
        return MagicMock(**kw)

    def __init__(self, data):
        super().__init__()
        self._data = data

    def metadata(self):
        return {"props": {"partitioned": True}}

    def __contains__(self, key):
        return key in self._data

    def __getitem__(self, key):
        return self._data[key]


def test_docname():
    assert Database.docname("some-string") == "some-string"

    slot = Slot("slot-name", build_id=234)
    assert Database.docname(slot) == "nightly:slot-name:234"

    slot = Slot("test-slot", build_id=1)
    assert Database.docname(slot) == "testing:test-slot:1"

    slot = Slot("test-slot", build_id=1)
    slot.flavour = "dummy"
    assert Database.docname(slot) == "dummy:test-slot:1"

    assert Database.docname(("flavour", "name", 123)) == "flavour:name:123"


@pytest.mark.parametrize("key", [42, (1, 2), (1, 2, 3, 4)])
def test_docname_bad_arg(key):
    with pytest.raises(TypeError):
        Database.docname(key)


@patch(
    "datetime.date",
    **{"today.return_value": datetime.date(2020, 3, 27)},
)
def test_add_with_id(date):
    slot = Slot("slot-name", build_id=234)
    db = MagicMock()

    Database(db).add(slot)

    db.create_document.assert_called_once_with(
        {
            "_id": Database.docname(slot),
            "type": "slot-info",
            "slot": slot.name,
            "build_id": slot.build_id,
            "date": "2020-03-27",
            "config": slot.toDict(),
        },
        throw_on_exists=True,
    )


@patch(
    "datetime.date",
    **{"today.return_value": datetime.date(2020, 3, 27)},
)
def test_add_new_slot(date):
    slot = Slot("slot-name")
    db = MagicMock()
    db.get_partitioned_view_result.return_value = []

    Database(db).add(slot)

    db.create_document.assert_called_once_with(
        {
            "_id": Database.docname(slot),
            "type": "slot-info",
            "slot": slot.name,
            "build_id": 1,
            "date": "2020-03-27",
            "config": slot.toDict(),
        },
        throw_on_exists=True,
    )

    assert slot.build_id == 1


@patch(
    "datetime.date",
    **{"today.return_value": datetime.date(2020, 3, 27)},
)
def test_add_new_slot_custom_flavour(date):
    slot = Slot("slot-name")
    slot.flavour = "testing"
    db = MagicMock()
    db.get_partitioned_view_result.return_value = []

    Database(db).add(slot)

    db.create_document.assert_called_once_with(
        {
            "_id": Database.docname(slot),
            "type": "slot-info",
            "slot": slot.name,
            "build_id": 1,
            "date": "2020-03-27",
            "config": slot.toDict(),
        },
        throw_on_exists=True,
    )

    assert slot.build_id == 1


@patch(
    "datetime.date",
    **{"today.return_value": datetime.date(2020, 3, 27)},
)
def test_add_slot(date):
    slot = Slot("slot-name")
    db = MagicMock()
    db.get_partitioned_view_result.return_value = [{"value": [{"max": 10}, None]}]

    s = Database(db).add(slot)

    db.create_document.assert_called_once_with(
        {
            "_id": Database.docname(slot),
            "type": "slot-info",
            "slot": slot.name,
            "build_id": 11,
            "date": "2020-03-27",
            "config": slot.toDict(),
        },
        throw_on_exists=True,
    )

    assert s is slot
    assert slot.build_id == 11


@patch(
    "datetime.date",
    **{"today.return_value": datetime.date(2020, 3, 27)},
)
def test_add_slot_race(date):
    slot = Slot("slot-name")
    db = MagicMock()
    db.get_partitioned_view_result.return_value = [{"value": [{"max": 10}, None]}]

    def mock_create(doc, throw_on_exists=False):
        if doc["build_id"] < 12:
            # pretend somebody else created the slot with the build_id we wanted
            if throw_on_exists:
                raise CloudantDatabaseException(409, "exists")
        return Mock()

    db.create_document.side_effect = mock_create

    Database(db).add(slot)

    db.create_document.assert_called_with(
        {
            "_id": Database.docname(slot),
            "type": "slot-info",
            "slot": slot.name,
            "build_id": 12,
            "date": "2020-03-27",
            "config": slot.toDict(),
        },
        throw_on_exists=True,
    )

    assert slot.build_id == 12


@patch(
    "datetime.date",
    **{"today.return_value": datetime.date(2020, 3, 27)},
)
def test_add_slot_race_and_fail(date):
    slot = Slot("slot-name")
    db = MagicMock()
    db.get_partitioned_view_result.return_value = [{"value": [{"max": 10}, None]}]

    def mock_create(doc, throw_on_exists=False):
        if doc["build_id"] < 12:
            # pretend somebody else created the slot with the build_id we wanted
            if throw_on_exists:
                raise CloudantDatabaseException(409, "exists")
        raise CloudantDatabaseException(
            500, "very bad"
        )  # instead of succeeding, something bad happens

    db.create_document.side_effect = mock_create

    with pytest.raises(CloudantDatabaseException):
        Database(db).add(slot)

    # FIXME: this is really what we want? should the slot be unchanged?
    assert slot.build_id == 12


def test_add_with_existing_id():
    slot = Slot("slot-name", build_id=234)
    db = MagicMock()
    db.create_document.side_effect = HTTPError("Conflict")

    with pytest.raises(HTTPError):
        Database(db).add(slot)


def test_wrong_db_type():
    data = MagicMock(metadata=MagicMock())
    data.metadata.return_value = {"props": {"partitioned": False}}

    with pytest.raises(AssertionError):
        db = Database(data)


def test_accessors():
    slot = Slot("some-slot", build_id=1)

    db = Database(MockDBData({"nightly:some-slot:1": "stuff"}))

    assert db.exists(slot)
    assert not db.exists("nightly:some-slot:2")

    assert slot in db
    assert "nightly:some-slot:2" not in db

    assert db[slot] == "stuff"


def test_apply():
    func = Mock()
    doc = Mock()

    Database.apply(func, doc)

    func.assert_called_once_with(doc)
    doc.save.assert_called_once()
    doc.fetch.assert_not_called()


def test_apply_retry():
    func = Mock()
    doc = Mock()

    # exceptions to raise
    errors = [
        HTTPError("409 Client Error: Conflict conflict ..."),
        HTTPError("409 Client Error: Conflict conflict ..."),
    ]

    def fail_once():
        if errors:
            raise errors.pop()

    doc.save.side_effect = fail_once

    Database.apply(func, doc)

    assert not errors
    func.assert_has_calls([call(doc), call(doc), call(doc)])
    assert doc.save.call_count == 3
    assert doc.fetch.call_count == 2


@pytest.mark.parametrize(
    "error", [HTTPError("500 whatever ..."), RuntimeError("some problem")]
)
def test_apply_unhandled_exceptions(error):
    from requests import HTTPError

    func = Mock()
    doc = Mock()
    doc.save.side_effect = error

    with pytest.raises(error.__class__):
        Database.apply(func, doc)


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_checkout_start(gethostname, datetimeMock):
    slot = Slot("some-slot", build_id=1, projects=[Project("Gaudi", "master")])

    class DocMock(dict):
        pass

    doc = DocMock(checkout={})
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.checkout_start(slot.Gaudi, "my_worker_task_id")

    assert "checkout" in db["nightly:some-slot:1"]
    assert "projects" in db["nightly:some-slot:1"]["checkout"]
    assert "Gaudi" in db["nightly:some-slot:1"]["checkout"]["projects"]

    proj = db["nightly:some-slot:1"]["checkout"]["projects"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
    }

    doc.save.assert_called_once()


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_checkout_start_empty(gethostname, datetimeMock):
    slot = Slot("some-slot", build_id=1, projects=[Project("Gaudi", "master")])

    class DocMock(dict):
        pass

    doc = DocMock()
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.checkout_start(slot.Gaudi, "my_worker_task_id")

    assert "checkout" in db["nightly:some-slot:1"]
    assert "projects" in db["nightly:some-slot:1"]["checkout"]
    assert "Gaudi" in db["nightly:some-slot:1"]["checkout"]["projects"]

    proj = db["nightly:some-slot:1"]["checkout"]["projects"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
    }

    doc.save.assert_called_once()


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_checkout_start_previous(gethostname, datetimeMock):
    slot = Slot("some-slot", build_id=1, projects=[Project("Gaudi", "master")])

    class DocMock(dict):
        pass

    doc = DocMock(
        checkout={"projects": {"Gaudi": {"data": "some previous checkout data"}}}
    )
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.checkout_start(slot.Gaudi, "my_worker_task_id")

    assert "checkout" in db["nightly:some-slot:1"]
    assert "projects" in db["nightly:some-slot:1"]["checkout"]
    assert "Gaudi" in db["nightly:some-slot:1"]["checkout"]["projects"]

    proj = db["nightly:some-slot:1"]["checkout"]["projects"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
        "previous": {"data": "some previous checkout data"},
    }

    doc.save.assert_called_once()

    # multiple previous checkouts
    db.checkout_start(slot.Gaudi, "my_new_worker_task_id")

    proj = db["nightly:some-slot:1"]["checkout"]["projects"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_new_worker_task_id",
        "previous": {
            "started": str(datetimeMock.now()),
            "hostname": gethostname(),
            "worker_task_id": "my_worker_task_id",
            "previous": {"data": "some previous checkout data"},
        },
    }


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 10)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_checkout_complete(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[
            Project("Gaudi", "master"),
            DBASE(packages=[Package("PRConfig", "master")]),
        ],
    )
    start_time = str(datetime.datetime(2020, 3, 27, 1, 2, 0))

    class DocMock(dict):
        pass

    doc = DocMock(
        checkout={
            "projects": {
                "Gaudi": {
                    "started": start_time,
                    "hostname": gethostname(),
                    "worker_task_id": "my_worker_task_id",
                },
                "PRConfig": {
                    "started": start_time,
                    "hostname": gethostname(),
                    "worker_task_id": "my_worker_task_id",
                },
            }
        }
    )
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    report = Mock()
    report.records = [{"level": "warning", "text": "a warning message"}]
    db.checkout_complete(
        slot.Gaudi,
        "checkout/Gaudi/d3/d308e36d35e209eadf72f7c7544750ae336d260edf33426ad5ab02a94d10dbc5.zip",
        report,
        "my_worker_task_id",
    )

    assert "checkout" in db["nightly:some-slot:1"]
    assert "projects" in db["nightly:some-slot:1"]["checkout"]
    assert "Gaudi" in db["nightly:some-slot:1"]["checkout"]["projects"]

    proj = db["nightly:some-slot:1"]["checkout"]["projects"]["Gaudi"]
    assert proj == {
        "started": start_time,
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
        "completed": str(datetime.datetime.now()),
        "merges": report.merges,
        "submodules": report.submodules,
        "tree": report.tree,
        "errors": [],
        "warnings": ["a warning message"],
        "artifact": slot.Gaudi.artifacts("checkout"),
        "dependencies": [],
    }

    report = Mock()
    report.records = []
    db.checkout_complete(
        slot.DBASE.PRConfig, "PRConfig-artifact.zip", report, "my_worker_task_id"
    )
    assert "PRConfig" in db["nightly:some-slot:1"]["checkout"]["projects"]

    proj = db["nightly:some-slot:1"]["checkout"]["projects"]["PRConfig"]
    assert proj == {
        "started": start_time,
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
        "completed": str(datetime.datetime.now()),
        "merges": report.merges,
        "submodules": report.submodules,
        "tree": report.tree,
        "errors": [],
        "warnings": [],
        "artifact": "PRConfig-artifact.zip",
        "dependencies": [],
    }


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 10)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_checkout_worker_task_id(gethostname, datetimeMock):
    slot = Slot("some-slot", build_id=1, projects=[Project("Gaudi", "master")])
    start_time = str(datetime.datetime(2020, 3, 27, 1, 2, 0))

    class DocMock(dict):
        pass

    doc = DocMock(
        checkout={
            "projects": {
                "Gaudi": {
                    "started": start_time,
                    "hostname": gethostname(),
                    "worker_task_id": "my_worker_task_id",
                }
            }
        }
    )
    doc.save = Mock()
    db = Database(MockDBData({"nightly:some-slot:1": doc}))
    report = Mock()
    with pytest.raises(AssertionError):
        db.checkout_complete(
            slot.Gaudi, "artifact.zip", report, "my_other_worker_task_id"
        )


def test_reuse_checkout():
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[
            Project("Gaudi", "master"),
            DBASE(packages=[Package("PRConfig", "master")]),
        ],
    )
    old_summary = {
        "started": "2022-03-18 16:15:27.664539",
        "hostname": "example.org",
        "worker_task_id": "my_worker_task_id",
        "completed": "2022-03-18 16:15:32.664539",
        "merges": [],
        "submodules": [],
        "tree": "abc",
        "errors": [],
        "warnings": ["a warning message"],
        "artifact": "something.zip",
        "dependencies": ["Upstream"],
    }

    class DocMock(dict):
        pass

    doc = DocMock(config=slot.toDict())
    doc.save = Mock()

    mock_db = MockDBData({"nightly:some-slot:1": doc})
    mock_db.get_view_result().all.return_value = [{"value": old_summary}]

    db = Database(mock_db)

    assert db.reuse_checkout(slot.Gaudi, "something.zip")

    assert (
        db["nightly:some-slot:1"]["config"]["projects"][0]["dependencies"]
        == old_summary["dependencies"]
    )

    # try the case where the artifacts is not known
    mock_db.get_view_result().all.return_value = []
    assert not db.reuse_checkout(slot.Gaudi, "something.zip")


def test_reuse_artifact():
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[
            Project("Gaudi", "master"),
            DBASE(packages=[Package("PRConfig", "master")]),
        ],
        platforms=["x86_64-centos7-gcc9-opt"],
    )
    old_summary = {
        "started": "2022-03-18 16:15:27.664539",
        "hostname": "example.org",
        "worker_task_id": "my_worker_task_id",
        "completed": "2022-03-18 16:15:32.664539",
        "merges": [],
        "submodules": [],
        "tree": "abc",
        "errors": [],
        "warnings": ["a warning message"],
        "artifact": "something.zip",
        "dependencies": ["Upstream"],
    }

    class DocMock(dict):
        pass

    doc = DocMock(config=slot.toDict())
    doc.save = Mock()

    mock_db = MockDBData({"nightly:some-slot:1": doc})
    mock_db.get_view_result().all.return_value = [{"value": old_summary}]

    db = Database(mock_db)

    assert db.reuse_artifact(slot.Gaudi, "something.zip")

    assert (
        db["nightly:some-slot:1"]["config"]["projects"][0]["dependencies"]
        == old_summary["dependencies"]
    )

    # try the case where the artifacts is not known
    mock_db.get_view_result().all.return_value = []
    assert not db.reuse_artifact(slot.Gaudi, "something.zip")

    # try build artifact
    old_build_summary = {
        "started": "2022-03-18 16:15:27.664539",
        "hostname": "example.org",
        "worker_task_id": "my_worker_task_id",
        "completed": "2022-03-18 16:15:32.664539",
        "warnings": 1,
        "errors": 1,
        "retcode": 0,
        "artifact": "build_artifact.zip",
    }

    doc = DocMock(
        builds={"x86_64-centos7-gcc9-opt": {"Gaudi": old_build_summary}},
        tests={"x86_64-centos7-gcc9-opt": {"Gaudi": old_build_summary}},
    )
    doc.save = Mock()

    mock_db = MockDBData({"nightly:some-slot:1": doc})
    mock_db.get_view_result().all.return_value = [{"value": old_build_summary}]

    db = Database(mock_db)

    assert db.reuse_artifact(
        slot.Gaudi,
        "build_artifact.zip",
        platform="x86_64-centos7-gcc9-opt",
        stage="build",
    )
    assert db.reuse_artifact(
        slot.Gaudi,
        "build_artifact.zip",
        platform="x86_64-centos7-gcc9-opt",
        stage="test",
    )
    mock_db.get_view_result().all.return_value = []
    assert not db.reuse_artifact(
        slot.Gaudi,
        "build_artifact.zip",
        platform="x86_64-centos7-gcc9-opt",
        stage="build",
    )
    assert not db.reuse_artifact(
        slot.Gaudi,
        "build_artifact.zip",
        platform="x86_64-centos7-gcc9-opt",
        stage="test",
    )


def test_set_dependencies():
    slot = Slot("some-slot", build_id=1, projects=[Project("Gaudi", "master")])

    class DocMock(dict):
        pass

    doc = DocMock(config=slot.toDict())
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    slot.Gaudi.dependencies = Mock(return_value=["a", "b", "c"])

    db.set_dependencies(slot.Gaudi)

    slot.Gaudi.dependencies.assert_called_once()
    doc.save.assert_called_once()

    assert "config" in db["nightly:some-slot:1"]
    assert "projects" in db["nightly:some-slot:1"]["config"]
    assert db["nightly:some-slot:1"]["config"]["projects"][0]["name"] == "Gaudi"
    assert (
        db["nightly:some-slot:1"]["config"]["projects"][0]["dependencies"]
        == slot.Gaudi.dependencies.return_value
    )


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2021, 1, 28, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_build_start(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )

    class DocMock(dict):
        pass

    doc = DocMock(builds={})
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.build_start(slot.Gaudi, slot.platforms[0], "my_worker_task_id")

    assert "builds" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["builds"]
    assert "Gaudi" in db["nightly:some-slot:1"]["builds"][slot.platforms[0]]

    proj = db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
    }

    doc.save.assert_called_once()


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2021, 1, 28, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_build_start_empty(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )

    class DocMock(dict):
        pass

    doc = DocMock()
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.build_start(slot.Gaudi, slot.platforms[0], "my_worker_task_id")

    assert "builds" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["builds"]
    assert "Gaudi" in db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]

    proj = db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
    }

    doc.save.assert_called_once()


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_build_start_previous(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )

    class DocMock(dict):
        pass

    doc = DocMock(
        builds={slot.platforms[0]: {"Gaudi": {"data": "some previous build data"}}}
    )
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.build_start(slot.Gaudi, slot.platforms[0], "my_worker_task_id")

    assert "builds" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["builds"]
    assert "Gaudi" in db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]

    proj = db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
        "previous": {"data": "some previous build data"},
    }

    doc.save.assert_called_once()

    # multiple previous builds
    db.build_start(slot.Gaudi, slot.platforms[0], "my_new_worker_task_id")

    proj = db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_new_worker_task_id",
        "previous": {
            "started": str(datetimeMock.now()),
            "hostname": gethostname(),
            "worker_task_id": "my_worker_task_id",
            "previous": {"data": "some previous build data"},
        },
    }


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 10)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_build_complete(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )
    start_time = str(datetime.datetime(2020, 3, 27, 1, 2, 0))

    class DocMock(dict):
        pass

    doc = DocMock(
        builds={
            "x86_64-centos7-gcc9-opt": {
                "Gaudi": {
                    "started": start_time,
                    "hostname": gethostname(),
                    "worker_task_id": "my_worker_task_id",
                }
            }
        }
    )
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    reports = {
        "warnings": 1,
        "errors": 1,
        "retcode": 0,
        "reports": {"configure": Mock(), "build": Mock()},
    }
    reports["reports"]["configure"].records = [
        {"level": "warning", "text": "a warning message"}
    ]
    reports["reports"]["build"].records = [
        {"level": "error", "text": "an error message"}
    ]

    db.build_complete(
        slot.Gaudi,
        slot.platforms[0],
        slot.Gaudi.artifacts("build", slot.platforms[0]),
        reports,
        "my_worker_task_id",
    )

    assert "builds" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["builds"]
    assert "Gaudi" in db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]

    proj = db["nightly:some-slot:1"]["builds"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": start_time,
        "hostname": gethostname(),
        "worker_task_id": "my_worker_task_id",
        "completed": str(datetime.datetime.now()),
        "warnings": 1,
        "errors": 1,
        "retcode": 0,
        "artifact": slot.Gaudi.artifacts("build", slot.platforms[0]),
    }


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 10)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_build_worker_task_id(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )
    start_time = str(datetime.datetime(2020, 3, 27, 1, 2, 0))

    class DocMock(dict):
        pass

    doc = DocMock(
        builds={
            "x86_64-centos7-gcc9-opt": {
                "Gaudi": {
                    "started": start_time,
                    "hostname": gethostname(),
                    "worker_task_id": "my_worker_task_id",
                }
            }
        }
    )
    doc.save = Mock()
    db = Database(MockDBData({"nightly:some-slot:1": doc}))
    reports = {"configure": Mock(), "build": Mock()}

    with pytest.raises(AssertionError):
        db.build_complete(
            slot.Gaudi,
            slot.platforms[0],
            slot.Gaudi.artifacts("build", slot.platforms[0]),
            reports,
            "my_other_task_id",
        )


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2021, 1, 28, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_tests_start(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )

    class DocMock(dict):
        pass

    doc = DocMock(tests={})
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.tests_start(slot.Gaudi, slot.platforms[0], "my_task_id")

    assert "tests" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["tests"]
    assert "Gaudi" in db["nightly:some-slot:1"]["tests"][slot.platforms[0]]

    proj = db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_task_id",
    }

    doc.save.assert_called_once()


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2021, 1, 28, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_tests_start_empty(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )

    class DocMock(dict):
        pass

    doc = DocMock()
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.tests_start(slot.Gaudi, slot.platforms[0], "my_task_id")

    assert "tests" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["tests"]
    assert "Gaudi" in db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]

    proj = db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_task_id",
    }

    doc.save.assert_called_once()


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 3)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_tests_start_previous(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )

    class DocMock(dict):
        pass

    doc = DocMock(
        tests={slot.platforms[0]: {"Gaudi": {"data": "some previous build data"}}}
    )
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    db.tests_start(slot.Gaudi, slot.platforms[0], "my_task_id")

    assert "tests" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["tests"]
    assert "Gaudi" in db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]

    proj = db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_task_id",
        "previous": {"data": "some previous build data"},
    }

    doc.save.assert_called_once()

    # multiple previous builds
    db.tests_start(slot.Gaudi, slot.platforms[0], "my_new_task_id")

    proj = db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": str(datetimeMock.now()),
        "hostname": gethostname(),
        "worker_task_id": "my_new_task_id",
        "previous": {
            "started": str(datetimeMock.now()),
            "hostname": gethostname(),
            "worker_task_id": "my_task_id",
            "previous": {"data": "some previous build data"},
        },
    }


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 10)},
)
@patch("lb.nightly.db.database.gethostname", return_value="example.org")
def test_tests_complete(gethostname, datetimeMock):
    slot = Slot(
        "some-slot",
        build_id=1,
        projects=[Project("Gaudi", "master")],
        platforms=["x86_64-centos7-gcc9-opt"],
    )
    start_time = str(datetime.datetime(2020, 3, 27, 1, 2, 0))

    class DocMock(dict):
        pass

    doc = DocMock(
        tests={
            "x86_64-centos7-gcc9-opt": {
                "Gaudi": {
                    "started": start_time,
                    "hostname": gethostname(),
                    "worker_task_id": "my_task_id",
                }
            }
        }
    )
    doc.save = Mock()

    db = Database(MockDBData({"nightly:some-slot:1": doc}))

    reports = {"test": Mock(), "results": {"PASS": ["a", "b"], "FAIL": ["c"]}}

    db.tests_complete(
        slot.Gaudi,
        slot.platforms[0],
        slot.Gaudi.artifacts("test", slot.platforms[0]),
        reports,
        "my_task_id",
    )

    assert "tests" in db["nightly:some-slot:1"]
    assert "x86_64-centos7-gcc9-opt" in db["nightly:some-slot:1"]["tests"]
    assert "Gaudi" in db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]

    proj = db["nightly:some-slot:1"]["tests"]["x86_64-centos7-gcc9-opt"]["Gaudi"]
    assert proj == {
        "started": start_time,
        "hostname": gethostname(),
        "worker_task_id": "my_task_id",
        "completed": str(datetime.datetime.now()),
        "artifact": slot.Gaudi.artifacts("test", slot.platforms[0]),
        "results": {"PASS": ["a", "b"], "FAIL": ["c"]},
    }
