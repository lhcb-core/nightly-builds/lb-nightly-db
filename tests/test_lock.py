###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import datetime
from unittest.mock import DEFAULT, MagicMock, patch

import pytest
from cloudant.error import CloudantDatabaseException

from lb.nightly.db.database import Database
from lb.nightly.db.utils import LockTakenError


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 3)},
)
def test_lock_simple(datetimeMock):
    db_instance = MagicMock()

    db = Database(db_instance)
    with db.lock("some-test") as d:
        assert d == "lock:some-test"

        db_instance.create_document.assert_called_once_with(
            {
                "_id": d,
                "type": "lock",
                "acquired": "2020-03-27 01:02:03",
                "info": None,
            },
            throw_on_exists=True,
        )

    db_instance["lock:some-test"].delete.assert_called_once()


@patch(
    "datetime.datetime",
    **{"now.return_value": datetime.datetime(2020, 3, 27, 1, 2, 3)},
)
def test_lock_with_info(datetimeMock):
    db_instance = MagicMock()

    db = Database(db_instance)
    with db.lock("some-test", {"some": "data"}) as d:
        assert d == "lock:some-test"

        db_instance.create_document.assert_called_once_with(
            {
                "_id": d,
                "type": "lock",
                "acquired": "2020-03-27 01:02:03",
                "info": {"some": "data"},
            },
            throw_on_exists=True,
        )

    db_instance["lock:some-test"].delete.assert_called_once()


def test_lock_raise():
    db_instance = MagicMock()

    db_instance.create_document.side_effect = [
        CloudantDatabaseException(409, "exists"),
    ]

    db = Database(db_instance)
    with pytest.raises(LockTakenError):
        with db.lock("some-test"):
            pass
